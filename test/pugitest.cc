#include "dom.h"
#include "httpwrapper.h"

#include <sstream>
#include <iostream>
#include <vector>

#include <glog/logging.h>

using namespace tcrawler;
using namespace pugi;
using namespace std;

int main(int argc, char **argv) {

	google::InitGoogleLogging(argv[0]);

	ostringstream oss;
	oss << "<!DOCTYPE html>" << endl
			<< "<html>" << endl
			<< "<body>" << endl
			<< "Great"
			<< "<h1>Example" << endl
			<< "<div>" << endl
			<< "<p class='papa'>Hello Word</p>" << endl
			<< "<p>This is <a href='www.test.com'>Hello World</a></p>" << endl
			<< "Great"
			<< "</body>" << endl
			<< "</html>" << endl;

	XPathSelector selector;
	string fixed = Tidy::Fix(oss.str());
	cout << fixed << endl;
	selector.LoadXml(oss.str());
	xpath_node h1 = selector.SelectSingleNode("/html/body/h1[1]");
	cout << h1.node().text().get() << endl;

	xpath_node root = selector.SelectSingleNode("/html");
	cout << root.node().text().get() << endl;

	xpath_node body = XPathSelector::SelectSingleNode(root.node(), "body");
	cout << body.node().text().get() << endl;

	vector<xpath_node> nodes = XPathSelector::Select(body.node(), "*/*");
	cout << "====== output of text under body ======" << endl;
	for (vector<xpath_node>::iterator itr = nodes.begin(); itr != nodes.end(); itr++) {
		cout << itr->node().text().get() << "===";
	}
	cout << endl;

	cout << "=======================================" << endl;
	cout << HtmlUtility::GetInnerText(body.node()) << endl;

	cout << "=======================================" << endl;
	xpath_node cl = selector.SelectSingleNode("/html/body/p/@class");
	cout << cl.attribute().value() << endl;
}
