#include <pcrecpp.h>
#include <string>
#include <iostream>

#include "httpwrapper.h"

using pcrecpp::RE;
using std::string;
using std::cout;
using std::endl;

using namespace tcrawler;

int main(int argc, char **argv) {
	string str = "hello #(age)";
	string dynamicPrefix = "((?:[^#]|^)(?:##)*)#\\(age\\)";

	cout << RE(dynamicPrefix).GlobalReplace("\\1Chris", &str) << endl;
	cout << str << endl;

	string html = "<head><meta charset=\"utf-8\" /><meta name=\"Description\" content";
	string encoding = HtmlUtility::ParsePageEncoding(html);
	cout << "Page encoding is " << encoding << endl;
}
