#include "phase.h"
#include "attrtree.h"
#include <iostream>

using namespace tcrawler;
using namespace clcommon;
using namespace pugi;
using namespace std;

int main(int argc, char **argv) {

	// ========== Load By Xml ===========
	// This leads to memory leak, try to manually load the regex
	// node and test whether SetText leaks memory.
	xml_document doc;
	LoadXmlDocument("../../test/templates/parse.xml", doc);

	xml_node first = doc.first_child().first_child();
	CLPtr<AttributedNode> tree = CreateTreeNode(first);
	tree->SerializeToStream(cout, 0);

	CLPtr<Phase> parse = PhaseParseContent::Create(tree);

	if (parse) {
		cout << "Complete." << endl;
	}

	//CLPtr<PhaseParseContent> parse = new PhaseParseContent();
	//parse->SetRegexContent("Name:\\s*(?<name>\\S+)\\s+Age:\\s*(?<age>\\d+)");
	//parse->SetSave(true);

	CLPtr<Context> context = new Context();
	string text = "Name:  Chris Age:24Name:Helena Age:   34 Name:Paul Age:12Name:PAge:345";
	context->SetLastResponse(text);
	parse->Run(context);

	cout << "=====================" << endl;
	for (Context::output_iterator itr = context->OutputBegin();
	     itr != context->OutputEnd(); itr++) {
		cout << (*itr)->size() << endl;
	}
}
