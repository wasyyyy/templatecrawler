#include "regex.h"
#include "clptr.h"
#include <iostream>

using namespace std;
using namespace tcrawler;
using namespace clcommon;

int main(int argc, char **argv) {
	string pattern = "Name (?<name>\\S+), age (?<age>\\d+) (?<hehe>.)";
	vector<string> names = RegexHelper::GetGroupNames(pattern);

	cout << "====== Names ======" << endl;
	for (vector<string>::iterator itr = names.begin(); itr != names.end(); itr++) {
		cout << *itr << endl;
	}

	cout << "====== Matches ======" << endl;
	string text = "Name Chris, age 24 yabcdName Helena, age 22 p";
	CLPtr<KVBarn> matches = RegexHelper::Matches(text, pattern);

	cout << matches->size() << " entries returned." << endl;

	for (KVBarn::iterator single = matches->begin();
	     single != matches->end(); single++) {
		for (KVStore::iterator itr = (*single)->begin();
	       itr != (*single)->end(); itr++) {
			cout << itr->first << " -> " << itr->second << endl;
		}
		cout << "---------------------" << endl;
	}
}
