#include "phase.h"
#include "attrtree.h"
#include <iostream>

using namespace std;
using namespace clcommon;
using namespace tcrawler;
using namespace pugi;

int main(int argc, char **argv) {
	xml_document doc;
	LoadXmlDocument("../../test/templates/get.xml", doc);

	// Access the Get phase.
	xml_node root = doc.first_child().first_child();
	CLPtr<AttributedNode> tree = CreateTreeNode(root);

	tree->SerializeToStream(cout, 0);

	CLPtr<Phase> get = PhaseGet::Create(tree);

	CLPtr<Context> context = new Context();

	get->Run(context);
}
