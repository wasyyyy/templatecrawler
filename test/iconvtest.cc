#include "codec.h"
#include <glog/logging.h>
#include <iostream>

using namespace clcommon;
using namespace std;

int main(int argc, char** argv) {

	google::InitGoogleLogging(argv[0]);

	string source = "你好wo shi 克里斯 Lin";
	string target;
	string back;

	Encoding encoding("utf8", "gb2312");
	int result = encoding.Convert(source, target);
	result = encoding.From("gb2312").To("utf8").Convert(target, back);

	cout << "Before converting: " << source << ", size: " << source.length() << endl;
	cout << "After converting: " << target << ", size: " << target.length() << endl;
	cout << "Converted back: " << back << ", size: " << back.length() << endl;

	// use valgrind to check whether there is any memory leak.
	encoding.From("utf8").To("gb2312");
	for (int i = 0; i < 100; i++) {
		encoding.Convert(source, target);
	}
	
	return 0;
}
