#define DEBUG

#include "attrtree.h"
#include <iostream>

using namespace clcommon;
using namespace pugi;
using namespace std;

void printName2(xml_node& xmlNode, CLPtr<AttributedNode>& treeNode, int dep) {
	string name(xmlNode.name());
	cout << string(dep, ' ') << name;
	treeNode->SetName(name);
	
	for (xml_node::attribute_iterator itr = xmlNode.attributes_begin(); itr != xmlNode.attributes_end(); itr++) {
		cout << "--" << string(itr->name());
		treeNode->SetAttribute(string(itr->name()), string(itr->value()));
	}

	cout << "|" << string(xmlNode.value()) << "|" << string(xmlNode.text().get());
	cout << endl;

	for (xml_node::iterator itr = xmlNode.begin(); itr != xmlNode.end(); itr++) {
		if (itr->type() == node_element) {
			CLPtr<AttributedNode> child = new AttributedNode();
			printName2(*itr, child, dep + 1);
		}
	}
}

//CLPtr<AttributedNode> printName(xml_node& node, int dep) {
void printName(xml_node& node, int dep) {

	//CLPtr<AttributedNode> thisNode(new AttributedNode());
	//CLPtr<AttributedNode> thisNode;
	//AttributedNode *pNode = new AttributedNode();
	//delete pNode;
	//char *chs = new((void*)0x970010)char[100];
	//cout << "MMMMMMM:chs->" << reinterpret_cast<void*>(chs) << endl;
	//node.print(cout);

	string name(node.name());
	cout << string(dep, ' ') << name;
	//thisNode->SetName(name);

	for (xml_node::attribute_iterator itr = node.attributes_begin(); itr != node.attributes_end(); itr++) {
		cout << "--" << string(itr->name());
		//CLPtr<Attribute> attr = new Attribute(string(itr->name()), string(itr->value()));
		//thisNode->SetAttribute(attr);
	}

	cout << "|" << string(node.value());
	cout << "|" << string(node.text().get());

	cout << endl;

	for (xml_node::iterator itr = node.begin(); itr != node.end(); itr++) {
		if (itr->type() == node_element) {
			printName(*itr, dep + 1);
			//CLPtr<AttributedNode> ret = printName(*itr, dep + 1);
			//thisNode->AppendChild(ret);
		}
	}
	
	//return CLPtr<AttributedNode>();
	//return thisNode;
}

int main(int argc, char **argv) {
	cout << "Loading" << endl;
	xml_document doc;
	LoadXmlDocument("../../test/templates/test.xml", doc);

	//CLPtr<AttributedNode> node = new AttributedNode();
	//CLPtr<AttributedNode> node = printName(doc, 0);
	xml_node node = doc.first_child();
	//printName(node, 0);

	//node->SetName("Hello");
	//node->SetText("Great");
	//node->SetAttribute("Name", "Chris");
	//node->SetAttribute("Age", "24");
	//node->SerializeToStream(cout, 0);

	//printName2(doc, node, 0);
	//node->SerializeToStream(cout, 0);

	//return 0;

	cout << "Printing" << endl;
	node.print(cout);
	cout << "Creating tree node" << endl;
	CLPtr<AttributedNode> root = CreateTreeNode(node);
	cout << "Outputing" << endl;
	root->SerializeToStream(cout, 0);
}
