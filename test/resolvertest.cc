#include "resolver.h"
#include "provider.h"

#include <iostream>

using clcommon::CLPtr;
using namespace tcrawler;
using namespace std;

int main(int argc, char **argv) {

	// ================= Resolve Test ======================
	CLPtr<Provider> provider = new StringProvider();

	provider->Set("name", "Chris");
	provider->Set("job", "SDE");
	provider->Set("age", "24");
	provider->Set("company", "Google");
	provider->Set("url", "http://www.baidu.com");

	cout << "Name: " << provider->Get("name") << endl;
	cout << "Job: " << provider->Get("job") << endl;
	cout << "Age: " << provider->Get("age") << endl;
	cout << "Company: " << provider->Get("company") << endl;
	cout << "Url: " << provider->Get("url") << endl;

	string candidate = "My name is #(name), I am #(age) year-old and currently working as a #(job) at #(company).";
	string staticCandidate = "Try to download @(url)";
	string failedCandidate = "Hello #(world)!";
	
	cout << endl << "Before resolving:" << endl;
	cout << candidate << endl;
	cout << staticCandidate << endl;
	cout << failedCandidate << endl;

	cout << endl << "After resolving:" << endl;
	cout << BindingResolver::Resolve(provider, candidate, Dynamic) << endl;
	cout << BindingResolver::Resolve(provider, staticCandidate, Static) << endl;
	cout << BindingResolver::Resolve(provider, failedCandidate, Dynamic) << endl;

	// ================== Numeric Resolve Test =======================
	string equation = "There are @($) apples on the ground.";
	cout << endl << "Numeric Resolver";
	cout << endl << "Before resolving:" << endl;
	cout << equation << endl;
	cout << endl << "After resolving:" << endl;
	cout << NumericResolver::Resolve(equation, 128) << endl;

	return 0;
}
