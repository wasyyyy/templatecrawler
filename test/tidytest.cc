#include "dom.h"
#include <sstream>
#include <iostream>

using std::ostringstream;
using std::cout;
using std::endl;
using std::cin;
using tcrawler::Tidy;

int main(int argc, char **argv) {
	ostringstream oss;
	oss << "<!DOCTYPE html>" << endl
			<< "<html>" << endl
			<< "<body>" << endl
			<< "<h1>Example" << endl
			<< "</body>" << endl
			<< "</html>" << endl;
	
	cout << endl << "Result:" << endl;
	cout << Tidy::Fix(oss.str()) << endl;
}
