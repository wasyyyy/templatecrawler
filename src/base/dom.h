#ifndef DOM_H_
#define DOM_H_

#include <string>
#include <vector>
#include <pugixml.hpp>

namespace tcrawler {
	class Tidy {
	public:
		static std::string Fix(const std::string &html);
	};

	class XPathSelector {
	public:
		void LoadXml(const std::string &xml, bool tidyFix = true);
		std::vector<pugi::xpath_node> Select(const std::string &xpath) const;
		pugi::xpath_node SelectSingleNode(const std::string &xpath) const;

		static std::vector<pugi::xpath_node> Select(const pugi::xml_node& node, const std::string& xpath);
		static pugi::xpath_node SelectSingleNode(const pugi::xml_node& node, const std::string& xpath);

	private:
		pugi::xml_document _document;
		bool _fixed;
	};
}

#endif
