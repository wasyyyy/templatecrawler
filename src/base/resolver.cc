#include "resolver.h"
#include "parser.h"
#include <pcrecpp.h>
#include <set>

#include <glog/logging.h>

using namespace std;
using namespace clcommon;

using pcrecpp::RE;
using pcrecpp::StringPiece;

using clcommon::CLPtr;

static string staticNameExtractor = "(?:[^@]|^)(?:@@)*@\\((\\w+)\\)";
static string staticPrefix = "((?:[^@]|^)(?:@@)*)@\\(";

static string dynamicNameExtractor = "(?:[^#]|^)(?:##)*#\\((\\w+)\\)";
static string dynamicPrefix = "((?:[^#]|^)(?:##)*)#\\(";

static string numericPatternExtractor = "(?:[^@]|^)(?:@@)*@\\(([+\\-*/0-9]*\\$[+\\-*/0-9]*)\\)";
static string numericPrefix = "((?:[^@]|^)(?:@@)*)@\\(";

static string Escape(const string& raw) {
	string unescaped = raw;
	RE("(\\+|\\$|\\*)").GlobalReplace("\\\\\\1", &unescaped);
	return unescaped;
}

static string GenerateHolder(const string& prefix, const string& name) {
	return prefix + Escape(name) + "\\)";
}

string tcrawler::BindingResolver::ResolveStatic(const CLPtr<Provider>& provider, const string& raw) {
	return ResolveInternal(provider, raw, staticNameExtractor, staticPrefix);
}

string tcrawler::BindingResolver::ResolveDynamic(const CLPtr<Provider>& provider, const string& raw) {
	return ResolveInternal(provider, raw, dynamicNameExtractor, dynamicPrefix);
}

string tcrawler::BindingResolver::ResolveInternal(const CLPtr<Provider>& provider, const string& raw, const string& extractor, const string& prefix) {
	RE re(extractor);
	StringPiece input(raw);
	string name;

	set<string> keys;

	while (re.FindAndConsume(&input, &name)) {
		keys.insert(name);
		LOG(INFO) << "Key " << name << " inserted.";
	}

	string resolved = raw;
	for (set<string>::const_iterator itr = keys.cbegin(); itr != keys.cend(); itr++) {
		string value = provider->Get(*itr);
		string replacee = GenerateHolder(prefix, *itr);
		LOG(INFO) << "Holder " << replacee;
		RE(replacee).GlobalReplace("\\1" + value, &resolved);
	}

	return resolved;
}

string tcrawler::BindingResolver::Resolve(const CLPtr<Provider>& provider, const string& raw, BindType type) {

	if (!provider) {
		return raw;
	}
	switch (type) {
		case Static:
			return ResolveStatic(provider, raw);
		case Dynamic:
			return ResolveDynamic(provider, raw);
	}

	return string();
}

bool tcrawler::BindingResolver::Unresolved(const string& raw) {
	return RE(staticNameExtractor).PartialMatch(raw) || RE(dynamicNameExtractor).PartialMatch(raw);
}

static int ComputeEquation(const string& equation) {
	int result;
	if (!Parser::ParseInt(equation, result)) {
		result = -1;
		LOG(ERROR) << "Failed to compute equation " << equation;
	}

	return result;
}

static int ComputeEquationWithSub(const string& equation, int sub) {
	string eq = equation;
	RE("\\$").GlobalReplace(to_string(sub), &eq);
	return ComputeEquation(eq);
}

string tcrawler::NumericResolver::Resolve(const string& raw, int number) {
	RE re(numericPatternExtractor);
	StringPiece input(raw);
	string equation;

	set<string> equations;

	LOG(INFO) << "Finding equations in " << raw;
	while (re.FindAndConsume(&input, &equation)) {
		equations.insert(equation);
		LOG(INFO) << "Equation " << equation << " inserted.";
	}
	
	string resolved = raw;
	for (set<string>::const_iterator itr = equations.cbegin(); itr != equations.cend(); itr++) {
		int value = ComputeEquationWithSub(*itr, number);
		string replacee = GenerateHolder(numericPrefix, *itr);
		LOG(INFO) << "Holder " << replacee;
		RE(replacee).GlobalReplace("\\1" + to_string(value), &resolved);
	}

	return resolved;
}
