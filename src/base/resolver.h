#ifndef RESOLVER_H_
#define RESOLVER_H_

#include <string>

#include "provider.h"

namespace tcrawler {

	enum BindType {
		Static,
		Dynamic,
	};
	
	class BindingResolver {
	public:
		static std::string Resolve(const clcommon::CLPtr<Provider>& provider, const std::string& raw, BindType bind);
	
		static std::string ResolveStatic(const clcommon::CLPtr<Provider>& provider, const std::string& raw);
		static std::string ResolveDynamic(const clcommon::CLPtr<Provider>& provider, const std::string& raw);
	
		static bool Unresolved(const std::string& raw);

	private:
		static std::string ResolveInternal(const clcommon::CLPtr<Provider>& provider, const std::string& raw, const std::string& extractor, const std::string& prefix);
	};

	class NumericResolver {
	public:
		static std::string Resolve(const std::string& raw, int number);
	};
}

#endif
