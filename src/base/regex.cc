#include "regex.h"
#include <pcre.h>
#include <pcrecpp.h>

#include <glog/logging.h>

using namespace std;
using namespace pcrecpp;
using namespace clcommon;
using namespace tcrawler;

// First two bytes represent the back reference number
// later bytes are zero-terminated C style string.
static bool ParseNameEntry(const char* entry, int& refNumber, string& name) {
	refNumber = *((unsigned int *)entry);
	name = string(entry + 2);
}

vector<string> RegexHelper::GetGroupNames(const string& pattern) {
	const char* errmsg;
	int erroffset;
	pcre* re = pcre_compile(pattern.c_str(), 0, &errmsg, &erroffset, NULL);
	pcre_extra* extra = pcre_study(re, 0, &errmsg);

	int groupCount;
	int entrySize;
	char* names;
	pcre_fullinfo(re, extra, PCRE_INFO_NAMECOUNT, &groupCount);
	pcre_fullinfo(re, extra, PCRE_INFO_NAMEENTRYSIZE, &entrySize);
	pcre_fullinfo(re, extra, PCRE_INFO_NAMETABLE, &names);

	vector<string> ret;
	for (int i = 0, start = 0; i < groupCount; i++, start += entrySize) {
		int num;
		string name;
		ParseNameEntry(names + start, num, name);
		ret.push_back(name);
	}

	pcre_free(extra);
	pcre_free(re);

	return ret;
}

CLPtr<KVBarn> RegexHelper::Matches(const string& text, const string& pattern, int options) {
	pcre* re;
	const char* errmsg;
	int erroffset;
	CLPtr<KVBarn> results(NULL);

	//LOG(INFO) << "Compiling regex pattern " << pattern;
	re = pcre_compile(pattern.c_str(), PCRE_UTF8 | options, &errmsg, &erroffset, NULL);
	if (re == NULL) {
		LOG(ERROR) << "Failed while trying to compile regex pattern " << pattern;
		return results;
	}
	//re = pcre_compile(pattern.c_str(), 0, &errmsg, &erroffset, NULL);

	int offset = 0;
	bool fail = false;

	vector<string> names = GetGroupNames(pattern);
	results = CLPtr<KVBarn>(new KVBarn());
	LOG(INFO) << "Matching regex pattern " << pattern;
	while (!fail) {
		int retCode;
		int ovector[90];
		retCode = pcre_exec(re, NULL, text.c_str(), text.length(),
		                    offset, 0, ovector, 90);
		//LOG(INFO) << "Matched once, return code: " << retCode;
	
		CLPtr<KVStore> result = new KVStore();
		if (retCode != PCRE_ERROR_NOMATCH) {
			if (retCode == 0) {
				// Log warning
			}
			int stringCount = retCode > 0 ? retCode : 30;
			for (vector<string>::iterator itr = names.begin();
			     itr != names.end(); itr++) {
				const char* name;
				int len = pcre_get_named_substring(re, text.c_str(), ovector,
				               stringCount, itr->c_str(), &name);
				result->Set(*itr, string(name));
				LOG(INFO) << "PCRE_FREE " << &name;
				pcre_free_substring(const_cast<char*>(name));
			}
			offset = ovector[1];
			results->Add(result);
		} else {
			fail = true;
		}
	}

	pcre_free(re);

	return results;
}
