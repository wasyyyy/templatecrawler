#ifndef REGEX_H_
#define REGEX_H_

#include <string>
#include <map>
#include <vector>

#include "provider.h"

namespace tcrawler {
	class RegexHelper {
	public:
		static std::vector<std::string> GetGroupNames(const std::string& pattern);
		static clcommon::CLPtr<KVBarn> Matches(const std::string& text, const std::string& pattern, int options = 0);
	};
}

#endif
