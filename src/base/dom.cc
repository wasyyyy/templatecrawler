#include "dom.h"
#include <tidypp/document.hpp>
#include <tidypp/buffer.hpp>
#include <tidypp/option.hpp>

#include <iostream>
#include <glog/logging.h>

using std::string;
using std::vector;
using std::cout;
using std::endl;

using pugi::xml_document;
using pugi::xml_node;
using pugi::xpath_node_set;
using pugi::xpath_node;
using pugi::xml_parse_result;

using tidypp::buffer;
using tidypp::document;
using tidypp::option;
using tidypp::optionid;

string tcrawler::Tidy::Fix(const string &html) {
	document doc;
	buffer input;
	buffer output;
	buffer err;

	input.append(const_cast<char*>(html.c_str()), html.length() + 1);
	string fixed;

	try
	{
		// Set the output to be xhtml, otherwise it is not possible for the pugixml to
		// parse the html output since the <meta> and <link> tags do not have closing tags.
		// For a complete list of options, see <a>http://tidy.sourceforge.net/docs/quickref.html#output-xhtml</a>
		optionid xhtml = option::getidbyname("output-xhtml");
		optionid input_enc = option::getidbyname("input-encoding");
		optionid output_enc = option::getidbyname("output-encoding");
		optionid wrap = option::getidbyname("wrap");
		optionid force = option::getidbyname("force-output");
		doc.optsetbool(xhtml, true);
		doc.optsetvalue(input_enc, "utf8");
		doc.optsetvalue(output_enc, "utf8");
		doc.optsetint(wrap, 0);
		doc.optsetbool(force, true);

		//TODO provide options for Phase Get and Phase Post to specify unrecognized
		// tags.
		optionid blocklevel_tags = option::getidbyname("new-blocklevel-tags");
		doc.optsetvalue(blocklevel_tags, "section");

		// Set error output buffer, otherwise the error output(including warnings) will be
		// sent to standard display.
		doc.seterrorbuffer(err);

		//TODO parse would fail if there is any unrecognized tags in the html, find an option to
		// suppress this behavior.
		doc.parsebuffer(input);
		doc.cleanandrepair();
		doc.savebuffer(output);

		//TODO generate a string using the raw ptr, rethink this for unicode.
		// As a matter of fact, UTF-8 is fine.
		//Done. We use utf8 consistently as our internal representation of all strings.
		fixed = string((char*)output.ptr(), output.size());
	}
	catch (const tidypp::exception &e)
	{
		cout << e.what() << endl;
		LOG(WARNING) << "Tidy fix failed, error message: " << e.what();
		LOG(WARNING) << "Target html text: " << html;
		fixed = html;
	}

	//cout << "Tidy parse finished." << endl;
	//cout << "Document parsed, is " << (doc.isxhtml() ? "" : "not ") << "a xhtml." << endl;

	input.free();
	output.free();
	err.free();

	return fixed;
}

void tcrawler::XPathSelector::LoadXml(const string &xml, bool tidyFix) {
	_fixed = tidyFix;
	xml_parse_result result;
	if (tidyFix) {
		string fixed = Tidy::Fix(xml);
		//_document.load_buffer(fixed.c_str(), fixed.length());
		//size_t docType = fixed.find_first_of('\n');
		//cout << docType << endl;
		//string clean = fixed.substr(docType + 1);
		//cout << clean << endl;
		result = _document.load(fixed.c_str());
	} else {
		//_document.load_buffer(xml.c_str(), xml.length());
		result = _document.load(xml.c_str());
	}

	//cout << "Document loaded" << endl;
	if (!result) {
		cout << result.description() << endl;
	}
}

vector<xpath_node> tcrawler::XPathSelector::Select(const string &xpath) const {
	xpath_node_set set = _document.select_nodes(xpath.c_str());
	//cout << set.size() << " node(s) selected" << endl;
	vector<xpath_node> results;

	for (xpath_node_set::const_iterator itr = set.begin(); itr != set.end(); itr++) {
		results.push_back(*itr);
	}

	return results;
}

xpath_node tcrawler::XPathSelector::SelectSingleNode(const string &xpath) const {
	xpath_node_set set = _document.select_nodes(xpath.c_str());
	//cout << set.size() << " node(s) selected" << endl;
	return set.first();
}

vector<xpath_node> tcrawler::XPathSelector::Select(const xml_node& node, const string& xpath) {
	xpath_node_set set = node.select_nodes(xpath.c_str());
	//cout << set.size() << " node(s) selected" << endl;
	vector<xpath_node> results;

	for (xpath_node_set::const_iterator itr = set.begin(); itr != set.end(); itr++) {
		results.push_back(*itr);
	}

	return results;
}

xpath_node tcrawler::XPathSelector::SelectSingleNode(const xml_node& node, const string& xpath) {
	xpath_node_set set = node.select_nodes(xpath.c_str());
	//cout << set.size() << " node(s) selected" << endl;
	return set.first();
}
