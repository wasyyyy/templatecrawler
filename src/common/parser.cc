#include "parser.h"

#include <glog/logging.h>
#include <stdexcept>

using namespace std;

bool clcommon::Parser::ParseInt(const string& source, int& parsed) {
	try {
		parsed = stoi(source);
		return true;
	} catch (const exception& e) {
		LOG(ERROR) << e.what();
		return false;
	}
}

bool clcommon::Parser::ParseLong(const string& source, long& parsed) {
	try {
		parsed = stol(source);
		return true;
	} catch (const exception& e) {
		LOG(ERROR) << e.what();
		return false;
	}
}

bool clcommon::Parser::ParseFloat(const string& source, float& parsed) {
	try {
		parsed = stof(source);
		return true;
	} catch (const exception& e) {
		LOG(ERROR) << e.what();
		return false;
	}
}

bool clcommon::Parser::ParseDouble(const string& source, double& parsed) {
	try {
		parsed = stod(source);
		return true;
	} catch (const exception& e) {
		LOG(ERROR) << e.what();
		return false;
	}
}
