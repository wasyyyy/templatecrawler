#ifndef PARSER_H_
#define PARSER_H_

#include <string>

namespace clcommon {

class Parser {
public:
	static bool ParseInt(const std::string& source, int& parsed);
	static bool ParseLong(const std::string& source, long& parsed);
	static bool ParseFloat(const std::string& source, float& parsed);
	static bool ParseDouble(const std::string& source, double& parsed);
};

}

#endif
