#include "attrtree.h"
#include <sstream>

//#define DEBUG

#ifdef DEBUG
#include <iostream>
#endif

using namespace std;
using namespace clcommon;
using namespace pugi;


//================== Utility =========================

CLPtr<AttributedNode> clcommon::CreateTreeNode(xml_node& node) {
	CLPtr<AttributedNode> thisNode = new AttributedNode();

#ifdef DEBUG
	cout << "Creating a new tree node" << endl;
	node.print(cout);
#endif
	
	thisNode->SetName(string(node.name()));
	thisNode->SetText(string(node.text().get()));
	/*
	xml_text text = node.text();
	if (text) {
		thisNode->SetText(string(text.get()));
#ifdef DEBUG
		cout << "Text: " << thisNode->GetText() << endl;
#endif
	}*/


#ifdef DEBUG
	cout << "================ " << thisNode->GetName() << " ## " << node.type() << endl;
	cout << "Trying to set attributes." << endl;
#endif

	for (xml_node::attribute_iterator itr = node.attributes_begin();
			 itr != node.attributes_end(); itr++) {
		string name(itr->name());
		string value(itr->value());
#ifdef DEBUG
		cout << "Attribute: " << name << "=" << value << endl;
#endif
		thisNode->SetAttribute(name, value);
	}

#ifdef DEBUG
	cout << "Trying to create descendants." << endl;
#endif

	for (xml_node::iterator itr = node.begin(); itr != node.end(); itr++) {
#ifdef DEBUG
		//cout << "############### " << itr->type() << endl;
		//itr->print(cout);
#endif
		if (itr->type() == node_element) {
			CLPtr<AttributedNode> child = CreateTreeNode(*itr);
			thisNode->AppendChild(child);
		} /*else if (itr->type() == node_cdata || itr->type() == node_pcdata) {
			if (itr->text()) {
				//thisNode->SetText(itr->text().get());
#ifdef DEBUG
				cout << "Text: " << thisNode->GetText() << endl;
#endif
			}
		}*/
	}

	return thisNode;
}

bool clcommon::LoadXmlDocument(const string& file, xml_document& doc) {
	xml_parse_result result = doc.load_file(file.c_str());

#ifdef DEBUG
	if (!result) {
		cout << result.description() << endl;
		return false;
	} else {
		doc.save(cout);
	}
#endif

	return true;
}

//================== AttributedNode =========================

void clcommon::AttributedNode::SetAttribute(const CLPtr<Attribute>& attr) {
	string name = attr->GetName();
	auto itr = _attributes.find(name);
	if (itr == _attributes.end()) {
		_attributes[name] = attr;
		_attributesVector.push_back(attr);
	} else {
		itr->second->SetValue(attr->GetValue());
	}
}

void clcommon::AttributedNode::SetAttribute(const string& name, const string& value) {
	CLPtr<Attribute> attr = new Attribute(name, value);
	SetAttribute(attr);
}

void clcommon::AttributedNode::AppendChild(const CLPtr<AttributedNode>& node) {
	_children.push_back(node);
	map<string, CLPtr<AttributedNode> >::const_iterator itr = _fastAccess.find(node->GetName());
	if (itr == _fastAccess.cend()) {
		_fastAccess[node->GetName()] = node;
	}
}

CLPtr<Attribute> clcommon::AttributedNode::GetAttribute(const string& name) const {
	map<string, CLPtr<Attribute> >::const_iterator itr = _attributes.find(name);
	CLPtr<Attribute> attr;
	if (itr != _attributes.cend()) {
		attr = itr->second;
	}

	return attr;
}

string clcommon::AttributedNode::GetAttributeStringValue(const string& name) const {
	map<string, CLPtr<Attribute> >::const_iterator itr = _attributes.find(name);
	string ret;
	if (itr != _attributes.cend()) {
		ret = itr->second->GetValue();
	}

	return ret;
}

CLPtr<AttributedNode> clcommon::AttributedNode::GetChild(const string& name) const {
	map<string, CLPtr<AttributedNode> >::const_iterator itr = _fastAccess.find(name);
	if (itr != _fastAccess.cend()) {
		return itr->second;
	} else {
		return CLPtr<AttributedNode>(NULL);
	}
}

CLPtr<AttributedNode> clcommon::AttributedNode::GetChild(const int index) const {
	if (index < _children.size()) {
		return _children.at(index);
	} else {
		return CLPtr<AttributedNode>(NULL);
	}
}

void clcommon::AttributedNode::SerializeToStream(ostream& output, const int depth) {
	string pad(depth, ' ');
	output << pad << "|--( ";
	output << _name;

	if (_text.length() > 0) {
		output << ":" << _text;
	}

	output << " )" << endl;

	if (_attributes.size() > 0) {
		output << pad << "   [ ";
		for (attribute_iterator itr = AttributeBegin(); itr != AttributeEnd(); itr++) {
			CLPtr<Attribute> attr = *itr;
			output << attr->GetName() << "=" << attr->GetValue() << " ";
		}
		output << "]" << endl;
	}

	for (node_iterator itr = _children.begin(); itr != _children.end(); itr++) {
		(*itr)->SerializeToStream(output, depth + 1);
	}
}

//================== Attribute =========================

clcommon::Attribute::Attribute(const string& name, const string& value)
	: _name(name), _value(value), _ref(0) {}

int clcommon::Attribute::AsInt() const {
	stringstream ss;
	ss << _value;
	int ret = -1;
	ss >> ret;
	return ret;
}

bool clcommon::Attribute::AsBoolean() const {
	stringstream ss;
	ss << _value;
	bool ret;
	ss >> ret;
	return ret;
}

float clcommon::Attribute::AsFloat() const {
	stringstream ss;
	ss << _value;
	float ret;
	ss >> ret;
	return ret;
}

double clcommon::Attribute::AsDouble() const {
	stringstream ss;
	ss << _value;
	double ret;
	ss >> ret;
	return ret;
}
