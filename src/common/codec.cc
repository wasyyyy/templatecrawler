#include "codec.h"

#include <iconv.h>
#include <sstream>
#include <glog/logging.h>

using namespace std;
using namespace clcommon;

#define BUFFER_SIZE 262144

Encoding& Encoding::From(const string& from) {
	_from = from;
	return *this;
}

Encoding& Encoding::To(const string& to) {
	_to = to;
	return *this;
}

int Encoding::Convert(const string& source, string& target) {
	iconv_t cd = iconv_open(_to.c_str(), _from.c_str());

	if (cd == NULL) {
		LOG(ERROR) << "At least one of " << _from << " and " << _to << " is not supported by iconv.";
		iconv_close(cd);
		return -1;
	}

	char buffer[BUFFER_SIZE];
	char *inbuf = const_cast<char*>(source.c_str());
	char *outbuf = buffer;
	size_t inleft = source.length();
	size_t outleft = BUFFER_SIZE;
	stringstream stream;

	while (inleft > 0) {

		LOG(INFO) << "ICONV inleft: " << inleft << " outleft: " << outleft;
		size_t result = iconv(cd, &inbuf, &inleft, &outbuf, &outleft);

		// If iconv returns E2BIG, there is no need to expand the output buffer.
		// Because we can deal with the rest of the bytes in the following iterations.
		if (result == (size_t) -1 && errno != E2BIG) {
			LOG(ERROR) << "Error in iconv, error code: " << errno
			           << " Meesage: " << strerror(errno)
			           << " Position: " << source.length() - inleft;
			iconv_close(cd);
			return -1;
		}

		size_t converted = BUFFER_SIZE - outleft;
		stream.write(buffer, converted);
		outbuf = buffer;
		outleft = BUFFER_SIZE;
	}
	
	inbuf = outbuf = NULL;

	iconv_close(cd);
	target = stream.str();
	return 0;
}
