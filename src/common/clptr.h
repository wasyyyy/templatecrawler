#ifndef CLPTR_H_
#define CLPTR_H_

// This class is a modification of the CefRefPtr from the Google's Cef project.
// currently there is no threading issue so we do not use lock and any atomic
// operations here just to keep it simple and easy.

#include <stddef.h>
#include <pthread.h>

#define IMPLEMENT_REFCOUNTING() \
	public: \
		virtual int Ref() { \
			return __sync_add_and_fetch(&_ref, 1); \
		} \
		virtual int Release() { \
			int ret = __sync_sub_and_fetch(&_ref, 1); \
			if (ret == 0) { \
				delete this; \
			} \
			return ret; \
		} \
	private: \
		int _ref;

namespace clcommon {
	template<class T>
	class CLPtr {
		public:
			CLPtr() : _ptr(NULL) {
			}

			CLPtr(T* p) : _ptr(p) {
				if (_ptr) {
					_ptr->Ref();
				}
			}

			CLPtr(const CLPtr<T>& r) : _ptr(r._ptr) {
				if (_ptr) {
					_ptr->Ref();
				}
			}

			~CLPtr() {
				if (_ptr) {
					_ptr->Release();
				}
			}

			T* get() const { return _ptr; }
			operator T*() const { return _ptr; }
			T* operator ->() const { return _ptr; }

			CLPtr<T>& operator=(T* p) {
				if (p) {
					p->Ref();
				}
				if (_ptr) {
					_ptr->Release();
				}

				_ptr = p;
				return *this;
			}

			CLPtr<T>& operator=(const CLPtr<T>& r) {
				return *this = r._ptr;
			}

			void swap(T** p) {
				T* pp = *p;
				*p = _ptr;
				_ptr = pp;
			}

			void swap(CLPtr<T>& r) {
				swap(r._ptr);
			}
		
		private:
			T* _ptr;
	};
}

#endif
