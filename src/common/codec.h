#ifndef CODEC_H_
#define CODEC_H_

#include <string>

namespace clcommon {
	
	class Encoding {
	public:
		Encoding(const std::string& from, const std::string& to) : _from(from), _to(to) {}
		Encoding() : _from("gbk"), _to("utf8") {}

		Encoding& From(const std::string& from);
		Encoding& To(const std::string& to);

		int Convert(const std::string& source, std::string& target);
	
	private:
		std::string _from;
		std::string _to;
	};
}

#endif
