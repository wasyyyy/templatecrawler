#ifndef ATTRIBUTED_TREE_H_
#define ATTRIBUTED_TREE_H_

#include <string>
#include <vector>
#include <map>
#include <pugixml.hpp>

#include "clptr.h"

namespace clcommon {

	class AttributedNode;
	class Attribute;

	CLPtr<AttributedNode> CreateTreeNode(pugi::xml_node& node);
	bool LoadXmlDocument(const std::string& file, pugi::xml_document& doc);

	class AttributedTree {
		public:
			CLPtr<AttributedNode> GetRoot() const { return _root; }
			void SetRoot(const CLPtr<AttributedNode>& root) { _root = root; }
	
		private:
			CLPtr<AttributedNode> _root;

		IMPLEMENT_REFCOUNTING()
	};

	class AttributedNode {
		public:
			AttributedNode() : _ref(0) {}
			
			void SetAttribute(const CLPtr<Attribute>& attr);
			void SetAttribute(const std::string& name, const std::string& value);
			
			void AppendChild(const CLPtr<AttributedNode>& node);

			void SetText(const std::string& text) { _text = text; }
			void SetName(const std::string& name) { _name = name; }

			CLPtr<Attribute> GetAttribute(const std::string& name) const;
			std::string GetAttributeStringValue(const std::string& name) const;
			
			typedef std::vector<CLPtr<Attribute> >::iterator attribute_iterator;
			attribute_iterator AttributeBegin() { return _attributesVector.begin(); }
			attribute_iterator AttributeEnd() { return _attributesVector.end(); }

			std::string GetText() const { return _text; }
			std::string GetName() const { return _name; }

			CLPtr<AttributedNode> GetChild(const std::string& name) const;
			CLPtr<AttributedNode> GetChild(const int index) const;
			
			typedef std::vector<CLPtr<AttributedNode> >::iterator node_iterator;
			node_iterator NodeBegin() { return _children.begin(); }
			node_iterator NodeEnd() { return _children.end(); }

			int GetChildrenCount() const { return _children.size(); }
			int GetAttributeCount() const { return _attributes.size(); }

			void SerializeToStream(std::ostream& output, const int depth);

		private:
			std::map<std::string, CLPtr<Attribute> > _attributes;
			std::vector<CLPtr<Attribute> > _attributesVector;

			std::vector<CLPtr<AttributedNode> > _children;
			std::map<std::string, CLPtr<AttributedNode> > _fastAccess;

			std::string _text;
			std::string _name;

		IMPLEMENT_REFCOUNTING()
	};

	class Attribute {
		public:
			Attribute(const std::string& name, const std::string& value);

			std::string GetName() const { return _name; }
			std::string GetValue() const { return _value; }

			void SetValue(const std::string& value) { _value = value; }

			int AsInt() const;
			bool AsBoolean() const;
			float AsFloat() const;
			double AsDouble() const;

		private:
			std::string _name;
			std::string _value;

		IMPLEMENT_REFCOUNTING()
	};
}

#endif
