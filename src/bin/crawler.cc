#include "template.h"
#include "factory.h"
#include "attrtree.h"
#include <pugixml.hpp>
#include <glog/logging.h>

#define DEBUG

#include <map>
#include <iostream>

using namespace std;
using namespace clcommon;
using namespace tcrawler;
using namespace pugi;

int main(int argc, char **argv) {
	if (argc <= 1) {
		cout << "Usage: " << argv[0] << " <path>" << endl;
		return 0;
	}

	google::InitGoogleLogging(argv[0]);
	//FLAGS_log_dir = "./logs";

	xml_document doc;
	LoadXmlDocument(argv[1], doc);
	xml_node node = doc.first_child();
	CLPtr<AttributedNode> tree = CreateTreeNode(node);
	cout << "Creating template..." << endl;
	CLPtr<Template> tpl = TemplateFactory::CreateTemplate(tree);

	cout << "Initializing provider..." << endl;
	CLPtr<Provider> provider = new StringProvider();

	cout << "Template initialized, Running..." << endl;
	CLPtr<TemplateResult> result = tpl->Run(provider);
	//tpl->Run(provider);

	cout << "Finished, Outputing..." << endl;
	/*
	for (TemplateResult::iterator itr = result->begin();
	     itr != result->end(); itr++) {
		for (KVStore::iterator i = (*itr)->begin();
		     i != (*itr)->end(); i++) {
			cout << i->first << " : " << i->second << endl;
		}
	}
	*/

	PhaseConstructorHolder::Dispose();
}
