#include "phase.h"
#include "attrtree.h"
#include "httpwrapper.h"
#include "regex.h"
#include "provider.h"
#include "factory.h"
#include "dom.h"
#include "codec.h"
#include "parser.h"

#include <pugixml.hpp>
#include <glog/logging.h>
#include <fstream>
#include <iostream>

#define DEBUG

using namespace clcommon;
using namespace std;
using namespace tcrawler;
using namespace pugi;

CLPtr<PhaseResult> tcrawler::PhaseGet::Run(CLPtr<Context>& context) {
	CurlWebClient curl;
	string url = _url;

#ifdef DEBUG
	cout << "Raw url: " << _url << endl;
#endif

	context->Resolve(url);

#ifdef DEBUG
	cout << "Curl requesting url: " << url << endl;
#endif

	string html = curl.Get(url);
	LOG(INFO) << "Fetched html length: " << html.length();
	LOG(INFO) << "Trying to fix corrupted html using Tidy.";
	string code = HtmlUtility::ParsePageEncoding(html);
	if (code != "utf8" && code != "utf-8") {
		string convert;
		Encoding(code, "utf8").Convert(html, convert);
		html = convert;
		LOG(INFO) << "Source page is encoded in " << code;
		LOG(INFO) << "Converted to utf8, size: " << convert.length();
	}
	//LOG(INFO) << html.substr(html.length() - 100);
	html = Tidy::Fix(html);
	//html = Tidy::Fix(html);

	context->SetLastUrl(url);
	context->SetLastResponse(html);

#ifdef DEBUG
	cout << "=== PhaseGet ===" << endl;
	cout << "Url: " << url << endl;
	cout << "Response Size: " << html.length() << endl;
	cout << "Response is written to INFO Log." << endl;
#endif

	CLPtr<PhaseResult> result = new PhaseResult();
	result->SetResponse(html);
	result->Successful = html.length() > 0;
	context->AddResult(result);
	context->SetLastResults(result);

	return result;
}

CLPtr<Phase> tcrawler::PhaseGet::Create(const CLPtr<AttributedNode>& phase) {
	CLPtr<AttributedNode> urlNode = phase->GetChild("Url");
	CLPtr<PhaseGet> get;
	
	if (urlNode) {
		get = new PhaseGet();
		get->_url = urlNode->GetText();
	}

#ifdef DEBUG
	cout << "====== PhaseGet ======" << endl;
	cout << "Created." << endl;
	cout << "Url: " << get->_url << endl;
	cout << "======================" << endl;
#endif

	return CLPtr<Phase>(get.get());
}

static CLPtr<KVBarn> ParseByXPath(const string& html, const string& baseXPath, const CLPtr<KVStore>& xpaths) {

	if (baseXPath.length() == 0 && xpaths->size() == 0) {
		LOG(WARNING) << "No xpath is provided with ParseByXPath.";
		return CLPtr<KVBarn>(NULL);
	}

	XPathSelector selector;
	selector.LoadXml(html);
	vector<xpath_node> bs = selector.Select(baseXPath);
	CLPtr<KVBarn> results = new KVBarn();

	for (vector<xpath_node>::iterator itr = bs.begin(); itr != bs.end(); itr++) {
		CLPtr<KVStore> item = new KVStore();
		for (KVStore::iterator xpath = xpaths->begin(); xpath != xpaths->end(); xpath++) {
			xml_node baseNode = itr->node();

			// BaseNodes must be xml_node rather than xml_attribute
			xpath_node target = XPathSelector::SelectSingleNode(baseNode, xpath->second);

			xml_node retNode = target.node();
			if (retNode.type() != node_null) {
				string text(HtmlUtility::GetInnerText(retNode));
				item->Set(xpath->first, text);
			} else {
				xml_attribute attr = target.attribute();
				string text(attr.value());
				item->Set(xpath->first, text);
			}
		}
		results->Add(item);
	}

	return results;
}

CLPtr<PhaseResult> tcrawler::PhaseParseContent::Run(CLPtr<Context>& context) {
	string html = context->GetLastResponse();
	CLPtr<PhaseResult> result = new PhaseResult();
	CLPtr<KVBarn> matches = NULL;

	if (html.length() > 0) {
		if (_regexContent.length() > 0) {
			string regexContent = _regexContent;
			context->Resolve(regexContent);
			matches =	RegexHelper::Matches(html, regexContent);
		} else {
			matches = ParseByXPath(html, _xpathBase, _xpaths);
		}
		result->Successful = false;
		if (matches->size() > 0) {
			result->Successful = true;
			for (KVBarn::iterator itr = matches->begin(); itr != matches->end(); itr++) {
				result->AddOutput(*itr);
			}
			if (_save) {
				for (KVBarn::iterator itr = matches->begin();
				     itr != matches->end(); itr++) {
					context->AddOutput(*itr);
				}
			}
		}
	}

	context->AddResult(result);
	context->SetLastResults(result);

	if (_listID.length() > 0) {
		context->AddBinding(_listID, matches);
	}

	return result;
}

CLPtr<Phase> tcrawler::PhaseParseContent::Create(const CLPtr<AttributedNode>& phase) {

	CLPtr<PhaseParseContent> parse = new PhaseParseContent();
	
	CLPtr<Attribute> saveAttr = phase->GetAttribute("Save");
	bool save = saveAttr->AsBoolean();
	parse->SetSave(save);

	string listID = phase->GetAttributeStringValue("ListID");
	parse->SetListID(listID);

	CLPtr<AttributedNode> regex = phase->GetChild("Regex");
	if (regex) {
		parse->SetRegexContent(regex->GetText());
	}

	CLPtr<AttributedNode> xpathBase = phase->GetChild("XpathBase");
	if (xpathBase) {
		parse->SetXPathBase(xpathBase->GetText());
		CLPtr<AttributedNode> xpaths = phase->GetChild("Xpath");
		for (AttributedNode::node_iterator itr = xpaths->NodeBegin();
				 itr != xpaths->NodeEnd(); itr++) {
			if ((*itr)->GetName() == "Item") {
				string name = (*itr)->GetAttributeStringValue("Name");
				string xpath = (*itr)->GetText();
				parse->AddXPath(name, xpath);
			}
		}
	}

#ifdef DEBUG
	cout << "====== PhaseParseContent ======" << endl;
	cout << "Created." << endl;
	cout << "Regex: " << parse->_regexContent << endl;
	cout << "===============================" << endl;
#endif

	return CLPtr<Phase>(parse.get());
}

CLPtr<PhaseResult> tcrawler::PhaseAdapter::Run(CLPtr<Context>& context) {
	CLPtr<KVBarn> bind = NULL;
	if (_bindID.length() > 0) {
		bind = context->GetBinding(_bindID);
	} else {
		CLPtr<PhaseResult> lastResult = context->GetLastResults();
		if (lastResult) {
			bind = lastResult->GetOutputs();
		}
	}

	CLPtr<PhaseResult> result = new PhaseResult();

	// There is no proper binding list, log the message and then return
	if (!bind) {
		result->Successful = false;
		LOG(WARNING) << "No proper binding list available (BindID: " << _bindID << ")";
		return result;
	}

	for (KVBarn::iterator itr = bind->begin(); itr != bind->end(); itr++) {
		CLPtr<Provider> provider = GetProviderByStore(*itr);
		context->PushDynamicProvider(provider);

		for (auto phase = _phases.begin(); phase != _phases.end(); phase++) {
			CLPtr<PhaseResult> pr = (*phase)->Run(context);
			CLPtr<KVBarn> barn = pr->GetOutputs();
			result->AddOutput(barn->begin(), barn->end());
		}

		context->PopDynamicProvider();
	}

	if (_listID.length() > 0) {
		context->AddBinding(_listID, result->GetOutputs());
	}

	context->AddResult(result);
	context->SetLastResults(result);

	return result;
}

CLPtr<Phase> tcrawler::PhaseAdapter::Create(const CLPtr<AttributedNode>& phase) {
	CLPtr<PhaseAdapter> adapter = new PhaseAdapter();

	string listID = phase->GetAttributeStringValue("ListID");
	string bindID = phase->GetAttributeStringValue("BindID");
	
	adapter->SetListID(listID);
	adapter->SetBindID(bindID);

	AttributedNode::node_iterator itr = phase->NodeBegin();
	for (; itr != phase->NodeEnd(); itr++) {
		CLPtr<Phase> child = PhaseFactory::CreatePhase(*itr);
		adapter->_phases.push_back(child);
	}

#ifdef DEBUG
	cout << "====== PhaseAdapter ======" << endl;
	cout << "ListID: " << listID << endl;
	cout << "BindID: " << bindID << endl;
	cout << "Subphases Count: " << adapter->_phases.size() << endl;
	cout << "==========================" << endl;
#endif

	return CLPtr<Phase>(adapter.get());
}

CLPtr<PhaseResult> tcrawler::PhasePrint::Run(CLPtr<Context>& context) {
	ostream* s = &cout;
	if (_file.length() > 0) {
		s = new ofstream(_file);
	}

	CLPtr<PhaseResult> result = new PhaseResult();
	result->Successful = false;

	ostream& output = *s;
	if (_bindID.length() > 0) {
		LOG(INFO) << "Print Binding, BindID: " << _bindID;
		CLPtr<KVBarn> barn = context->GetBinding(_bindID);
		output << barn << endl;
		output.flush();
		result->Successful = true;
	} else {
		LOG(INFO) << "Print Html";
		CLPtr<PhaseResult> last = context->GetLastResults();
		if (last) {
			string html = last->GetResponse();
			LOG(INFO) << "Html length: " << html.length();
			if (html.length() > 0) {
				output << html << endl;
				output.flush();
				result->Successful = true;
			}
		}
	}

	if (s != &cout) {
		delete s;
	}

	return result;
}

CLPtr<Phase> tcrawler::PhasePrint::Create(const CLPtr<AttributedNode>& phase) {
	CLPtr<PhasePrint> print = new PhasePrint();

	string bindID = phase->GetAttributeStringValue("BindID");
	CLPtr<Attribute> htmlAttr = phase->GetAttribute("PrintHtml");
	bool printHtml = htmlAttr ? htmlAttr->AsBoolean() : false;

	CLPtr<AttributedNode> fileNode = phase->GetChild("File");
	if (fileNode) {
		string filePath = fileNode->GetText();
		if (filePath.length() > 0) {
			print->SetFilePath(filePath);
		}
	}

	print->SetBindID(bindID);
	print->SetPrintHtml(printHtml);

#ifdef DEBUG
	cout << "====== PhasePrint ======" << endl;
	cout << "BindID: " << bindID << endl;
	cout << "PrintHtml: " << printHtml << endl;
	cout << "========================" << endl;
#endif

	return CLPtr<Phase>(print.get());
}

CLPtr<PhaseResult> tcrawler::PhaseList::Run(CLPtr<Context>& context) {
	string fromResolved = _from;
	string toResolved = _to;
	string stepResolved = _step;
	context->Resolve(fromResolved);
	context->Resolve(toResolved);
	context->Resolve(stepResolved);

	CLPtr<PhaseResult> result = new PhaseResult();

	int from, to, step;
	if (!(Parser::ParseInt(fromResolved, from) &&
	      Parser::ParseInt(toResolved, to) &&
				Parser::ParseInt(stepResolved, step))) {
		LOG(ERROR) << "from/to/step tags are not well formatted.";
		return result;
	}

	CLPtr<KVBarn> col = new KVBarn();
	for (int i = from; i <= to; i += step) {
		CLPtr<KVStore> item  = new KVStore();
		for (KVStore::iterator pattern = _patterns->begin(); pattern != _patterns->end(); pattern++) {
			string content = pattern->second;
			if (!context->Resolve(content) || !context->ResolveNumeric(content, i)) {
				LOG(ERROR) << "Failed to resolve " << pattern->second;
			} else {
				item->Set(pattern->first, content);
			}
		}
		col->Add(item);
	}

	if (_listID.length() > 0) {
		context->AddBinding(_listID, col);
	}

	result->AddOutput(col->begin(), col->end());
	context->AddResult(result);
	context->SetLastResults(result);
	
	return result;
}

CLPtr<Phase> tcrawler::PhaseList::Create(const CLPtr<AttributedNode>& phase) {
	
	CLPtr<PhaseList> list = new PhaseList();
	
	string listID = phase->GetAttributeStringValue("ListID");
	list->SetListID(listID);

	CLPtr<AttributedNode> fromNode = phase->GetChild("From");
	CLPtr<AttributedNode> toNode = phase->GetChild("To");
	CLPtr<AttributedNode> stepNode = phase->GetChild("Step");

	if (fromNode) { list->SetLoopStart(fromNode->GetText()); }
	if (toNode) { list->SetLoopEnd(toNode->GetText()); }
	if (stepNode) { list->SetLoopStep(stepNode->GetText()); }

	for (AttributedNode::node_iterator node = phase->NodeBegin(); node != phase->NodeEnd(); node++) {
		if ((*node)->GetName() == "Pattern") {
			string name = (*node)->GetAttributeStringValue("Name");
			if (name.length() > 0) {
				list->AddPattern(name, (*node)->GetText());
			} else {
				LOG(WARNING) << "Pattern without a name will be omitted quietly " << (*node)->GetText();
			}
		}
	}

	return CLPtr<Phase>(list.get());
}
