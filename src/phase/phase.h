#ifndef PHASE_H_
#define PHASE_H_

#include <string>
#include <vector>
#include <map>

#include "pugixml.hpp"
#include "context.h"
#include "attrtree.h"
#include "clptr.h"
#include "result.h"

namespace tcrawler {

	class Phase {
	public:
		virtual clcommon::CLPtr<PhaseResult> Run(clcommon::CLPtr<Context>& context) = 0;
		Phase() : _ref(0) {}
	
	IMPLEMENT_REFCOUNTING()
	};

	class PhaseGet : public Phase {
	public:
		PhaseGet() : _ref(0) {}
		virtual clcommon::CLPtr<PhaseResult> Run(clcommon::CLPtr<Context>& context);
	
		void SetUrl(std::string& url) { _url = url; }

		static clcommon::CLPtr<Phase> Create(const clcommon::CLPtr<clcommon::AttributedNode>& phase);

	private:
		std::string _url;
	
	IMPLEMENT_REFCOUNTING()
	};

	class PhaseParseContent : public Phase {
	public:
		PhaseParseContent() : _xpaths(new KVStore()), _ref(0) {}
		virtual clcommon::CLPtr<PhaseResult> Run(clcommon::CLPtr<Context>& context);

		void SetRegexContent(const std::string& regexContent) { _regexContent = regexContent; }
		void SetXPathBase(const std::string& xpathBase) { _xpathBase = xpathBase; }
		void SetXPath(const clcommon::CLPtr<KVStore>& xpaths) { _xpaths = xpaths; }

		void AddXPath(const std::string& name, const std::string& xpath) { _xpaths->Set(name, xpath); }

		void SetSave(bool save) { _save = save; }
		bool GetSave() const { return _save; }

		void SetListID(const std::string& listID) { _listID = listID; }

		static clcommon::CLPtr<Phase> Create(const clcommon::CLPtr<clcommon::AttributedNode>& phase);

	private:
		std::string _regexContent;
		std::string _xpathBase;
		clcommon::CLPtr<KVStore> _xpaths;
		bool _save;
		std::string _listID;
	
	IMPLEMENT_REFCOUNTING()
	};

	class PhaseAdapter : public Phase {
	public:
		PhaseAdapter() : _ref(0) {}
		virtual clcommon::CLPtr<PhaseResult> Run(clcommon::CLPtr<Context>& context);

		void SetBindID(const std::string& bindID) { _bindID = bindID; }
		void SetListID(const std::string& listID) { _listID = listID; }

		static clcommon::CLPtr<Phase> Create(const clcommon::CLPtr<clcommon::AttributedNode>& phase);

	private:
		std::string _bindID;
		std::string _listID;
		std::vector<clcommon::CLPtr<Phase> > _phases;

	IMPLEMENT_REFCOUNTING()
	};

	class PhasePrint : public Phase {
	public:
		PhasePrint() : _ref(0) {}
		virtual clcommon::CLPtr<PhaseResult> Run(clcommon::CLPtr<Context>& context);

		void SetBindID(const std::string& bindID) { _bindID = bindID; }
		void SetFilePath(const std::string& file) { _file = file; }
		void SetPrintHtml(bool printHtml) { _printHtml = printHtml; }

		static clcommon::CLPtr<Phase> Create(const clcommon::CLPtr<clcommon::AttributedNode>& phase);

	private:
		std::string _bindID;
		std::string _file;
		bool _printHtml;

	IMPLEMENT_REFCOUNTING()
	};

	class PhaseList : public Phase {
	public:
		PhaseList() : _ref(0), _patterns(new KVStore()), _from("1"), _to("1"), _step("1") {}
		virtual clcommon::CLPtr<PhaseResult> Run(clcommon::CLPtr<Context>& context);

		//void SetBindID(const std::string& bindID) { _bindID = bindID; }
		void SetListID(const std::string& listID) { _listID = listID; }
		void SetLoopStart(const std::string& from) { _from = from; }
		void SetLoopEnd(const std::string& to) { _to = to; }
		void SetLoopStep(const std::string& step) { _step = step; }

		void AddPattern(const std::string& name, const std::string& pattern) { _patterns->Set(name, pattern); }

		static clcommon::CLPtr<Phase> Create(const clcommon::CLPtr<clcommon::AttributedNode>& phase);

	private:
		//std::string _bindID;
		std::string _listID;

		std::string _from;
		std::string _to;
		std::string _step;
		clcommon::CLPtr<KVStore> _patterns;

	IMPLEMENT_REFCOUNTING()
	};
}

#endif
