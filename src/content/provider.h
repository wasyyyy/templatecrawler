#ifndef PROVIDER_H_
#define PROVIDER_H_

#include <memory>
#include <map>
#include <string>
#include <vector>

#include <iostream>

#include "clptr.h"

namespace tcrawler {
	class Provider {
	public:	
		virtual std::string Get(const std::string& name) const = 0;
		virtual void Set(const std::string& name, const std::string& value) = 0;
	
	IMPLEMENT_REFCOUNTING()
	};

	class FormattedProvider : public Provider {
	public:
		virtual int GetAsInt(std::string& name) const = 0;
		virtual float GetAsFloat(std::string& name) const = 0;
		virtual double GetAsDouble(std::string& name) const = 0;

	IMPLEMENT_REFCOUNTING()
	};

	class StringProvider : public Provider {
	public:
		StringProvider() : _ref(0) {}

		std::string Get(const std::string& name) const;
		void Set(const std::string& name, const std::string& value);

		typedef std::map<std::string, std::string>::iterator iterator;
		iterator begin() { return _values.begin(); }
		iterator end() { return _values.end(); }
		typedef std::map<std::string, std::string>::const_iterator const_iterator;

		typedef std::vector<std::string>::iterator key_iterator;
		key_iterator KeyBegin() { return _keys.begin(); }
		key_iterator KeyEnd() { return _keys.end(); }

		int size() const { return _values.size(); }

	private:
		std::map<std::string, std::string> _values;
		std::vector<std::string> _keys;
	
	IMPLEMENT_REFCOUNTING()
	};

	typedef StringProvider KVStore;

	inline clcommon::CLPtr<Provider> GetProviderByStore(const clcommon::CLPtr<KVStore>& store) {
		return clcommon::CLPtr<Provider>(static_cast<Provider*>(store.get()));
	}

	template<typename Collection>
	class Store {
	public:
		Store() : _ref(0) {}

		typedef typename std::vector<clcommon::CLPtr<Collection> >::iterator iterator;
		iterator begin() { return _storage.begin(); }
		iterator end() { return _storage.end(); }

		void Add(const clcommon::CLPtr<Collection>& col) { _storage.push_back(col); }
		void AddRange(iterator begin, iterator end) { _storage.insert(_storage.end(), begin, end); }
		void AddNamed(const std::string& name, const clcommon::CLPtr<Collection>& col);

		clcommon::CLPtr<Collection> GetByName(const std::string& name);

		int size() const { return _storage.size(); }

	private:
		std::vector<clcommon::CLPtr<Collection> > _storage;
		std::map<std::string, clcommon::CLPtr<Collection> > _fastAccess;
	
	IMPLEMENT_REFCOUNTING()
	};

	template<typename Collection>
	void Store<Collection>::AddNamed(const std::string& name, const clcommon::CLPtr<Collection>& col) {
		_storage.push_back(col);
		_fastAccess[name] = col;
	}

	template<typename Collection>
	clcommon::CLPtr<Collection> Store<Collection>::GetByName(const std::string& name) {
		clcommon::CLPtr<Collection> store = NULL;
		auto itr = _fastAccess.find(name);
		if (itr != _fastAccess.end()) {
			store = itr->second;
		}

		return store;
	}


	typedef Store<KVStore> KVBarn;
	typedef Store<KVBarn> KVHarbor;
	
/*
	inline void PrintKeyValuePair(const clcommon::CLPtr<KVStore>& store, const std::ostream& stream) {
		stream << "======== KV Store ========" << std::endl;
		for (KVStore::iterator itr = store->begin(); itr != store->end(); itr++) {
			stream << itr->first << " -> " << itr->second << std::endl;
		}
	}

	inline void PrintKeyValuePair(const clcommon::CLPtr<KVBarn>& barn, const std::ostream& stream) {
		stream << "======== KV Barn ========" << std::endl;
		for (KVBarn::iterator itr = barn->begin(); itr != barn->end(); itr++) {
			PrintKeyValuePair(*itr, stream);
		}
	}
*/
/*
	class KVBarn {
	public:
		typedef std::vector<clcommon::CLPtr<KVStore> >::iterator iterator;
		iterator begin() { return _barn.begin(); }
		iterator end() { return _barn.end(); }

		typedef std::vector<clcommon::CLPtr<KVStore> >::reverse_iterator reverse_iterator;
		reverse_iterator rbegin() { return _barn.rbegin(); }
		reverse_iterator rend() { return _barn.rend(); }

		void AddKVStore(const clcommon::CLPtr<KVStore>& store) { _barn.push_back(store); }
		void AddNamedKVStore(const std::string& name, const clcommon::CLPtr<KVStore>& store);

		clcommon::CLPtr<KVStore> GetKVStoreByName(const std::string& name);

		int size() const { return _barn.size(); }

	private:
		std::vector<clcommon::CLPtr<KVStore> > _barn;
		std::map<std::string, clcommon::CLPtr<KVStore> > _fastAccess;
	
	IMPLEMENT_REFCOUNTING()
	};
*/
	//TODO: add DateTime provider, segmented
	class DateTimeProvider : public Provider {
	IMPLEMENT_REFCOUNTING()
	};
}

std::ostream& operator << (std::ostream& stream, const clcommon::CLPtr<tcrawler::KVStore>& store);
std::ostream& operator << (std::ostream& stream, const clcommon::CLPtr<tcrawler::KVBarn>& barn);

#endif
