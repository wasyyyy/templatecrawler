#ifndef CONTEXT_H_
#define CONTEXT_H_

#include <string>
#include <list>
#include <vector>
#include <map>

#include "result.h"
#include "provider.h"
#include "clptr.h"

namespace tcrawler {
	class Context {
	public:
		Context() : _outs(new KVBarn()), _bindings(new KVHarbor()), _ref(0) {}

		// Provider
		void SetStaticProvider(const clcommon::CLPtr<Provider>& provider) { _static = provider; }
		void PushDynamicProvider(const clcommon::CLPtr<Provider>& provider);
		clcommon::CLPtr<Provider> PopDynamicProvider();

		// Last Results
		void SetLastResults(const clcommon::CLPtr<PhaseResult>& results) { _lastResult = results; }
		clcommon::CLPtr<PhaseResult> GetLastResults() const { return _lastResult; }

		// Last Response and Url
		void SetLastResponse(const std::string& html) { _lastResponse = html; }
		std::string GetLastResponse() const { return _lastResponse; }
		void SetLastUrl(const std::string& url) { _lastUrl = url; }
		std::string GetLastUrl() const { return _lastUrl; }

		// Phase related operations
		void AddResult(const clcommon::CLPtr<PhaseResult>& result) { _results.push_back(result); }
		void AddOutput(const clcommon::CLPtr<KVStore>& output) { _outs->Add(output); }
		void AddOutput(const std::string& name, const clcommon::CLPtr<KVStore>& output);
		clcommon::CLPtr<KVStore> GetNamedOutput(const std::string& name) { return _outs->GetByName(name); }
		void AddBinding(const std::string& name, const clcommon::CLPtr<KVBarn>& binding) { _bindings->AddNamed(name, binding); }
		clcommon::CLPtr<KVBarn> GetBinding(const std::string& name) { return _bindings->GetByName(name); }

		// Iteration through the result collection
		typedef KVBarn::iterator output_iterator;
		output_iterator OutputBegin() { return _outs->begin(); }
		output_iterator OutputEnd() { return _outs->end(); }
		clcommon::CLPtr<KVBarn> GetOutput() { return _outs; }

		bool Resolve(std::string& raw) const;
		bool ResolveNumeric(std::string& raw, int number) const;

	private:
		std::vector<clcommon::CLPtr<Provider> > _dynamics;
		clcommon::CLPtr<Provider> _static;

		std::string _lastResponse;
		std::string _lastUrl;

		clcommon::CLPtr<PhaseResult> _lastResult;
		std::list<clcommon::CLPtr<PhaseResult> > _results;

		clcommon::CLPtr<KVBarn> _outs;
		clcommon::CLPtr<KVHarbor> _bindings;

	IMPLEMENT_REFCOUNTING();
	};
}

#endif
