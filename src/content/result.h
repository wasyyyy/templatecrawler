#ifndef CONTENT_H_
#define CONTENT_H_

#include <memory>
#include <vector>

#include "provider.h"
#include "clptr.h"

namespace tcrawler {
	class TemplateResult {
 	public:
		TemplateResult() : _results(new KVBarn()), _ref(0) {}

		bool Successful;
		int ErrorCount;
		int TimeoutCount;
		int TotalCount;

		typedef std::vector<std::string>::iterator log_iterator;
		typedef std::vector<std::string>::const_iterator const_log_iterator;
		void AddPhaseLog(const std::string& log, bool recall);
		void AddPhaseLogBulk(const_log_iterator begin, const_log_iterator end);
		log_iterator LogBegin() { return _logs.begin(); }
		log_iterator LogEnd() { return _logs.end(); }

		void SetResults(const clcommon::CLPtr<KVBarn>& results) { _results = results; }

		typedef KVBarn::iterator iterator;
		iterator begin() { return _results->begin(); }
		iterator end() { return _results->end(); }

		void ClearCount() { ErrorCount = TimeoutCount = TotalCount = 0; }
		

	private:
		clcommon::CLPtr<KVBarn> _results;
		std::vector<std::string> _logs;

	IMPLEMENT_REFCOUNTING()
	};

	class PhaseResult {
	public:
		PhaseResult() : _extras(new KVStore()), _outputs(new KVBarn()),  _ref(0) {}

		bool Successful;
		int ErrorCount;
		int TimeoutCount;
		int TotalCount;

		std::string GetString(const std::string& name) const { return _extras->Get(name); }
		void SetString(const std::string& name, const std::string& value) { _extras->Set(name, value); }

		// Phase output
		typedef KVBarn::iterator iterator;
		void AddOutput(const clcommon::CLPtr<KVStore>& output) { _outputs->Add(output); }
		void AddOutput(iterator begin, iterator end) { _outputs->AddRange(begin, end); }
		iterator begin() { return _outputs->begin(); }
		iterator end() { return _outputs->end(); }
		clcommon::CLPtr<KVBarn> GetOutputs() { return _outputs; }
		
		// Phase logs
		typedef std::vector<std::string>::iterator log_iterator;
		typedef std::vector<std::string>::const_iterator const_log_iterator;
		void AddPhaseLog(const std::string& log);
		log_iterator LogBegin() { return _logs.begin(); }
		log_iterator LogEnd() { return _logs.end(); }

		// Clear counters
		void ClearCount() { ErrorCount = TimeoutCount = TotalCount = 0; }

		// Text responses
		std::string GetResponse() const { return _response; }
		void SetResponse(const std::string& response) { _response = response; }

	private:
		clcommon::CLPtr<KVStore> _extras;
		clcommon::CLPtr<KVBarn> _outputs;
		std::vector<std::string> _logs;
		std::string _response;
	
	IMPLEMENT_REFCOUNTING()
	};
}

#endif
