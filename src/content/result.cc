#include "result.h"
#include <glog/logging.h>

using namespace std;
using namespace tcrawler;
using namespace clcommon;

void PhaseResult::AddPhaseLog(const string& log) {
	_logs.push_back(log);
	LOG(INFO) << log;
}

void TemplateResult::AddPhaseLog(const string& log, bool recall = true) {
	_logs.push_back(log);
	LOG_IF(INFO, !recall) << log;
}

void TemplateResult::AddPhaseLogBulk(const_log_iterator begin, const_log_iterator end) {
	_logs.insert(_logs.end(), begin, end);
}
