#include "context.h"
#include "resolver.h"
#include "clptr.h"

using std::string;
using std::vector;
using std::list;

using namespace clcommon;

namespace tcrawler {

	void Context::PushDynamicProvider(const CLPtr<Provider>& provider) {
		_dynamics.push_back(provider);
	}

	CLPtr<Provider> Context::PopDynamicProvider() {
		if (_dynamics.empty()) {
			return CLPtr<Provider>(NULL);
		}

		CLPtr<Provider> result = _dynamics.back();
		_dynamics.pop_back();
		return result;
	}

	// Resolve a string with static binding or dynamic binding or both.
	// First we resolve static ones, and then dynamic ones packed in stack.
	bool Context::Resolve(string& raw) const {
		string unresolved = raw;

		unresolved = BindingResolver::Resolve(_static, unresolved, Static);

		for (vector<CLPtr<Provider> >::const_reverse_iterator itr = _dynamics.rbegin();
				 itr != _dynamics.rend(); itr++) {
			if (!BindingResolver::Unresolved(unresolved)) {
				break;
			}

			unresolved = BindingResolver::Resolve(*itr, unresolved, Dynamic);
		}

		if (BindingResolver::Unresolved(unresolved)) {
			return false;
		} else {
			raw = unresolved;
			return true;
		}
	}

	// Resolve numberic symbols in the raw string, formatted as @(numeric($))
	bool Context::ResolveNumeric(string& raw, int number) const {
		string unresolved = raw;
		unresolved = NumericResolver::Resolve(unresolved, number);
		if (unresolved.length() > 0) {
			raw = unresolved;
			return true;
		} else {
			return false;
		}
	}
}
