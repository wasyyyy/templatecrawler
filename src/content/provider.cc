#include "provider.h"
#include "clptr.h"

#include <iostream>

using namespace std;
using namespace tcrawler;
using namespace clcommon;

string tcrawler::StringProvider::Get(const string& name) const {
	map<string, string>::const_iterator val = _values.find(name);
	if (val != _values.end()) {
		return val->second;
	} else {
		return string();
	}
}

void tcrawler::StringProvider::Set(const string& name, const string& val) {
	_values[name] = val;
	auto itr = _values.find(name);
	if (itr == _values.end()) {
		_keys.push_back(name);
	}
}

static void PrintKVStore(ostream& stream, const CLPtr<KVStore>& store) {
	stream << "{";
	KVStore::iterator itr = store->begin();
	if (itr != store->end()) {
		stream << "\"" << itr->first << "\":\"" << itr->second << "\"";
		itr++;
	}
	for (; itr != store->end(); itr++) {
		stream << ",\"" << itr->first << "\":\"" << itr->second << "\"";
	}
	stream << "}";
}

ostream& operator << (ostream& stream, const CLPtr<KVStore>& store) {	
	stream << "======== KV Store ========" << endl;
	/*for (KVStore::iterator itr = store->begin(); itr != store->end(); itr++) {
		stream << itr->first << " -> " << itr->second << endl;
	}*/
	PrintKVStore(stream, store);
	return stream;
}

ostream& operator << (ostream& stream, const CLPtr<KVBarn>& barn) {
	stream << "======== KV Barn ========" << endl;
	for (KVBarn::iterator itr = barn->begin(); itr != barn->end(); itr++) {
		//clcommon::CLPtr<KVStore> store = *itr;
		//stream << store << endl;
		/*for (KVStore::iterator pair = (*itr)->begin(); pair != (*itr)->end(); pair++) {
			stream << pair->first << " -> " << pair->second << endl;
		}*/
		PrintKVStore(stream, *itr);
		stream << endl;
	}
	return stream;
}

