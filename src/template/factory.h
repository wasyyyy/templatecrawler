#ifndef FACTORY_H_
#define FACTORY_H_

#include <pugixml.hpp>
#include <map>
#include <memory>

#include "phase.h"
#include "template.h"
#include "clptr.h"

namespace tcrawler {

	typedef clcommon::CLPtr<Phase>(*PhaseCtor)(const clcommon::CLPtr<clcommon::AttributedNode>&);

	class PhaseConstructorHolder {
	public:
		// Only designed for one threaded appliation
		virtual ~PhaseConstructorHolder();
		static PhaseConstructorHolder* Instance();
		static void Dispose() { delete _instance; }
		PhaseCtor GetCreator(std::string& name);
		void SetCreator(std::string& name, PhaseCtor ctor);
	
	private:
		PhaseConstructorHolder() {}
		static PhaseConstructorHolder *_instance;
		std::map<std::string, PhaseCtor> _ctors;
	};

	class TemplateFactory {
	public:
		static clcommon::CLPtr<Template> CreateTemplate(const clcommon::CLPtr<clcommon::AttributedNode>& node);
	};

	class PhaseFactory {
	public:
		static clcommon::CLPtr<Phase> CreatePhase(const clcommon::CLPtr<clcommon::AttributedNode>& node);
	};
}

#endif
