#ifndef TEMPLATE_H_
#define TEMPLATE_H_

#include "result.h"
#include "context.h"
#include "clptr.h"
#include "phase.h"

#include <vector>

namespace tcrawler {

	class Template {
 	public:
  	clcommon::CLPtr<TemplateResult> Run(clcommon::CLPtr<Provider>& staticProvider);
		void AddPhase(clcommon::CLPtr<Phase>& phase) { _phases.push_back(phase); }
		//void AddJsonValue(Json::Value& value) { _results.push_back(value); }

	private:
		std::vector<clcommon::CLPtr<Phase> > _phases;
	
	IMPLEMENT_REFCOUNTING()
	};
}

#endif
