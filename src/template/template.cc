#include "template.h"
#include <glog/logging.h>

using tcrawler::TemplateResult;
using clcommon::CLPtr;
using std::vector;

#define DEBUG

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif

CLPtr<TemplateResult> tcrawler::Template::Run(CLPtr<Provider>& staticProvider) {
	CLPtr<Context> context = new Context();
	context->SetStaticProvider(staticProvider);

	LOG(INFO) << "Running template with " << _phases.size() << " phases.";

	for (vector<CLPtr<Phase> >::iterator itr = _phases.begin();
			 itr != _phases.end(); itr++) {

#ifdef DEBUG
		if (!(*itr)) {
			std::cout << "Phase value is null" << std::endl;
			throw "Phase value is null";
		}
		cout << "Running phase..." << endl;
#endif

		LOG(INFO) << "Running phase.";
		CLPtr<PhaseResult> result = (*itr)->Run(context);
		//context->AddResult(result);
	}

	CLPtr<TemplateResult> result = new TemplateResult();
	result->SetResults(context->GetOutput());
	return result;
}
