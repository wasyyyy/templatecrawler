#include "factory.h"
#include "attrtree.h"

#define DEBUG

#ifdef DEBUG
#include <iostream>
#endif

using namespace std;
using namespace clcommon;
using namespace tcrawler;

PhaseConstructorHolder* tcrawler::PhaseConstructorHolder::_instance = NULL;

PhaseConstructorHolder* tcrawler::PhaseConstructorHolder::Instance() {
	if (!_instance) {
		_instance = new PhaseConstructorHolder();
		_instance->_ctors["Get"] = PhaseGet::Create;
		_instance->_ctors["ParseContent"] = PhaseParseContent::Create;
		_instance->_ctors["Adapter"] = PhaseAdapter::Create;
		_instance->_ctors["Print"] = PhasePrint::Create;
		_instance->_ctors["List"] = PhaseList::Create;
	}

	return _instance;
}

tcrawler::PhaseConstructorHolder::~PhaseConstructorHolder() {
	_ctors.clear();
}

PhaseCtor tcrawler::PhaseConstructorHolder::GetCreator(string& name) {
	return _ctors.at(name);
}

void tcrawler::PhaseConstructorHolder::SetCreator(string& name, PhaseCtor ctor) {
	_ctors[name] = ctor;
}

CLPtr<Template> tcrawler::TemplateFactory::CreateTemplate(const CLPtr<AttributedNode>& node) {
	int size = node->GetChildrenCount();
#ifdef DEBUG
	cout << "Creating template containing " << size << " phases..." << endl;
#endif
	CLPtr<Template> temp = new Template();
	for (int i = 0; i < size; i++) {
		CLPtr<AttributedNode> child = node->GetChild(i);
		CLPtr<Phase> phase = PhaseFactory::CreatePhase(child);
		temp->AddPhase(phase);
	}

	return temp;
}

CLPtr<Phase> tcrawler::PhaseFactory::CreatePhase(const CLPtr<AttributedNode>& node) {
	string name = node->GetName();
	PhaseCtor ctor = PhaseConstructorHolder::Instance()->GetCreator(name);
	return ctor(node);
}
