#ifndef HTTP_WRAPPER_H_
#define HTTP_WRAPPER_H_

#include <pugixml.hpp>
#include <string>

namespace tcrawler {
	
	class HttpClient {
	public:
		virtual std::string Get(const std::string &url) = 0;
		virtual std::string Post(const std::string &url, const std::string &data) = 0;
	};

	enum HttpStatus {
		OK,
		HTTP404,
		HTTP50X,
	};

	class CurlWebClient : public HttpClient{
	public:
		std::string Get(const std::string &url);
		std::string Post(const std::string &url, const std::string &data);
	
		HttpStatus GetStatus() const { return _status; }

	private:
		HttpStatus _status;
	};

	class HtmlUtility {
	public:
		// We only deal with pages which have a ascii-compatible encoding for now. This is a
		// good practice adopted by most of the web pages.
		static std::string ParsePageEncoding(const std::string& html);
		static std::string GetInnerText(const pugi::xml_node& node);
	};
}

#endif
