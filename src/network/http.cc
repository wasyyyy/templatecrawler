#include "httpwrapper.h"
#include "provider.h"
#include "regex.h"

#include <stdlib.h>

#include <iostream>
#include <sstream>
#include <curl/curl.h>
#include <pcrecpp.h>
#include <glog/logging.h>

using namespace std;
using namespace pcrecpp;
using namespace clcommon;
using namespace pugi;

static size_t ReceiveContent(void *content, size_t size, size_t nmemb, void *user_ptr) {
	stringstream *stream = (stringstream *)user_ptr;
	size_t realsize = size * nmemb;
	string part((char *)content, realsize);
	(*stream) << part;
	LOG(INFO) << "Curl: " << realsize << " bytes received.";
	LOG(INFO) << "Received part length: " << part.length();
	//LOG(INFO) << part.substr(realsize - 100);
	//string tmp = (*stream).str();
	//LOG(INFO) << tmp.substr(tmp.length() - 100);
	//cout << str->length() << " bytes received." << endl;
	return realsize;
}

namespace tcrawler {
	using std::string;
	using std::cout;
	using std::endl;

	string CurlWebClient::Get(const string &url) {
		CURL *request = curl_easy_init();
		CURLcode retCode;

		stringstream html;
		string ret;

		if (request) {
			curl_easy_setopt(request, CURLOPT_URL, url.c_str());
			curl_easy_setopt(request, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(request, CURLOPT_USERAGENT,
				"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
			curl_easy_setopt(request, CURLOPT_WRITEFUNCTION, ::ReceiveContent);
			curl_easy_setopt(request, CURLOPT_WRITEDATA, (void *)&html);
			curl_easy_setopt(request, CURLOPT_ACCEPT_ENCODING, "gzip");
			curl_easy_setopt(request, CURLOPT_NOPROGRESS, 1L);
			curl_easy_setopt(request, CURLOPT_MAXREDIRS, 50L);
			curl_easy_setopt(request, CURLOPT_TCP_KEEPALIVE, 1L);

			retCode = curl_easy_perform(request);

			if (retCode == CURLE_OK) {
				//cout << html.length() << " bytes received." << endl;
				ret = html.str();
				LOG(INFO) << "CurlWebClient " << ret.length() << " bytes of html received.";
			} else {
				//cout << "CurlWebClient get failed!" << endl;
				LOG(WARNING) << "CurlWebClient get failed! Return code: " << retCode;
			}

			curl_easy_cleanup(request);
			//curl_free(request);
		}

		return ret;
	}

	string CurlWebClient::Post(const string &url, const string &data) {
		CURL *request = curl_easy_init();
		CURLcode retCode;

		string html;

		if (request) {
			curl_easy_setopt(request, CURLOPT_URL, url.c_str());
			curl_easy_setopt(request, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(request, CURLOPT_USERAGENT,
				"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
			curl_easy_setopt(request, CURLOPT_WRITEFUNCTION, ::ReceiveContent);
			curl_easy_setopt(request, CURLOPT_WRITEDATA, (void *)&html);
			curl_easy_setopt(request, CURLOPT_POSTFIELDS, data.c_str());

			retCode = curl_easy_perform(request);

			if (retCode == CURLE_OK) {
				cout << html.length() << " bytes received." << endl;
			} else {
				cout << "CurlWebClient post failed!" << endl;
				cout << retCode << endl;
			}

			curl_easy_cleanup(request);
		}

		return html;
	}

	static string CharsetRegex = "(?:meta.*?charset=|xml.*?encoding=)\"?(?<charset>[^\\s\">]+)\"?";
	//TODO The following regex pattern will cause double free or corrupted memory when calling RegexHelper::Matches
	// (?:meta.*?charset=\"?(?<charset>[^\\s\">]+)\"?)|(?:xml.*?encoding=\"?(?<charset2>[^\\s\">]+)\"?)
	// note the second captured group name is charset2, not charset.
	static int Options = PCRE_CASELESS | PCRE_MULTILINE;

	string HtmlUtility::ParsePageEncoding(const string& html) {
		
		string encoding;
		if (!RE(CharsetRegex, Options).PartialMatch(html, &encoding)) {
			cout << "No match, use UTF-8 as default" << endl;
			encoding = "utf8";
		}

		if (encoding == "utf-8") {
			encoding = "utf8";
		}

		// if gb2312 is captured, use gbk instead in case there is traditional Chinese character.
		if (encoding == "gb2312") {
			encoding = "gbk";
		}

		return encoding;
		
/*
		string encoding;
		cout << "Match start" << endl;
		CLPtr<KVBarn> matches = RegexHelper::Matches(html, CharsetRegex, Options);
		cout << "Match finished" << endl;

		if (!matches) {
			cout << "Match failed" << endl;
			return encoding;
		}

		KVBarn::iterator itr = matches->begin();
		if (itr != matches->end()) {
			KVStore::iterator pair = (*itr)->begin();
			if (pair != (*itr)->end()) {
				cout << "Matched, (" << pair->first << ", " << pair->second << ")" << endl;
				encoding = pair->second;
			} else {
				cout << "Failed" << endl;
			}
		} else {
			cout << "No match" << endl;
		}

		return encoding;*/
	}

	string HtmlUtility::GetInnerText(const xml_node& target) {
		class TextTraveller : public xml_tree_walker {
		public:
			virtual bool begin(xml_node& node) {
				text << node.text().get() << " ";
				return true;
			}

			virtual bool for_each(xml_node& node) {
				//TODO workaround for pugixml's bug, visiting same node twice.
				if (repeated) {
					text << node.text().get() << " ";
					LOG(INFO) << node.text().get();
				}
				repeated = !repeated;
				return true;
			}

			string GetText() const {
				return text.str();
			}

			TextTraveller() : repeated(false) {}

		private:
			stringstream text;
			bool repeated;
		};

		TextTraveller t;
		(const_cast<xml_node&>(target)).traverse(t);
		return t.GetText();
	}
}
