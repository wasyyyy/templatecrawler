CC=g++
LD=g++

INCLUDEBASE=$(TC)/include
SRC=$(TC)/src
TESTSRC=$(TC)/test
BUILD=$(TC)/build
#INCLUDE=-I$(INCLUDEBASE)/curl -I$(INCLUDEBASE)/pcre -I$(INCLUDEBASE)/pugixml -I$(INCLUDEBASE)
LIBPATH=-L$(TC)/libs

LIB=-lcurl -lpcrecpp -lpugixml -ltidypp-1.0 -lpcre -lglog
#INCLUDE=-I$(INCLUDEBASE)/pugixml -I$(INCLUDEBASE) -I$(SRC)/base -I$(SRC)/common -I$(SRC)/content -I$(SRC)/network -I$(SRC)/phase -I$(SRC)/template
INCLUDE=-I$(INCLUDEBASE) -I$(SRC)/base -I$(SRC)/common -I$(SRC)/content -I$(SRC)/network -I$(SRC)/phase -I$(SRC)/template

FLAGS=--std=c++11 -g -O0
#FLAGS=-g -O0
